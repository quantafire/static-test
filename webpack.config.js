const StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin');
const path = require("path")
const CustomComponent = require('./Custom').default
console.log({custom: CustomComponent})
const webpack = require('webpack')
const ReactDOMServer = require('react-dom/server')

module.exports = {

  entry: './Custom.js',

  output: {
    filename: 'done.js',
    path: __dirname + '/done',
    /* IMPORTANT!
     * You must compile to UMD or CommonJS
     * so it can be required in a Node context: */
    libraryTarget: 'umd',
    globalObject: 'this'
  },
  plugins: [
    new StaticSiteGeneratorPlugin({
      locals: {
        // Properties here are merged into `locals`
        // passed to the exported render function
        greet: 'Hello'
      }
    })
  ]
};