const gulp = require('gulp')
const each = require('gulp-each')
const babel = require('gulp-babel')
// const mfUiComponents = require('mf-ui-components')
const svgstore = require('gulp-svgstore')
const custom = require('./Custom.jsx')

gulp.task('markup', function() {
    return gulp.src('**.jsx')
        .pipe(babel({
            presets: ['@babel/preset-env', '@babel/preset-react']
        }))
        .pipe(gulp.dest('output'));
});