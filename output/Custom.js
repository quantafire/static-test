// my comment
const React = require('react')
module.exports =  {
    default: function CustomComponent(props) {
        return React.createElement("div", { title: props.title, className: "custom-class" })
    }
} 