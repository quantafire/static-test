// my comment
var gulp = require('gulp')
var each = require('gulp-each');

gulp.task('markup', function() {
    return gulp.src('*.js')
        .pipe(each(function(content, file, callback) {
            // content is a string containing the code
            // do with it as you'd like
            var newContent = '// my comment\n' + content;
 
            // file is the original Vinyl file object
 
            // return the new content using the callback
            // the first argument is an error, if you encounter one
            callback(null, newContent);
        }))
        .pipe(gulp.dest('output'));
});